package com.gitlab.jiehong.loops;

import java.util.List;

public class ReducePattern {
  public static void main(String[] args) {
    var data = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    loop(data);
    pattern(data);
  }

  private static void loop(final List<Integer> data) {
    int output = 0;
    for (final Integer element: data) {
      output = output + element;
    }
    System.out.println(output);
  }

  private static void pattern(final List<Integer> data) {
    int output = data.stream()
        .reduce(0, Integer::sum);
    System.out.println(output);
  }
}

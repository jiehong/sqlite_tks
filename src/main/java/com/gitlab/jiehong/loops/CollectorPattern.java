package com.gitlab.jiehong.loops;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectorPattern {
  public static void main(String[] args) {
    var data = List.of("hello", "yes", "it's", "me", "again", "sorry", "sorry");
    System.out.println(loop(data));
    System.out.println(pattern(data));
  }

  private static Set<String> loop(final List<String> data) {
    final Set<String> output = new HashSet<>();
    for (final String element: data) {
      final String value = element.toUpperCase(Locale.ROOT);
      output.add(value);
    }
    return output;
  }

  private static Set<String> pattern(final List<String> data) {
    return data.stream()
        .map(element -> element.toUpperCase(Locale.ROOT))
        .collect(Collectors.toSet());
  }
}

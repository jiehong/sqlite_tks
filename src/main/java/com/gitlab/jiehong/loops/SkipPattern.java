package com.gitlab.jiehong.loops;

import java.util.List;

public class SkipPattern {
  public static void main(String[] args) {
    var data = List.of(1, 2, 3, 3, 3, 4, 5, 6, 7, 8, 9, 10);
    loop(data);
    pattern(data);
  }

  private static void loop(final List<Integer> data) {
    long output = 0;
    for (int i = 0; i < data.size(); i++) {
      if (i < 8) {
        continue;
      }
      output++;
    }
    System.out.println(output);
  }

  private static void pattern(final List<Integer> data) {
    long output = data.stream()
        .skip(8)
        .count();
    System.out.println(output);
  }
}

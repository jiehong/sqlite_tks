package com.gitlab.jiehong.loops;

import java.util.Collection;
import java.util.List;

public class FlatMapPattern {
  public static void main(String[] args) {
    var data = List.of(List.of(1, 2, 3), List.of(4, 5), List.of(6));
    loop(data);
    pattern(data);
  }

  private static void loop(final List<List<Integer>> data) {
    for (final List<Integer> subList: data) {
      for (final Integer element: subList) {
        System.out.println(element);
      }
    }
  }

  private static void pattern(final List<List<Integer>> data) {
    data.stream()
        .flatMap(Collection::stream)
        .forEach(System.out::println);
  }
}

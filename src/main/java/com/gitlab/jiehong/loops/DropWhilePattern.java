package com.gitlab.jiehong.loops;

import java.util.List;

public class DropWhilePattern {
  public static void main(String[] args) {
    var data = List.of(1, 2, 3, 3, 3, 4, 5, 6, 7, 8, 9, 10);
    loop(data);
    pattern(data);
  }

  private static void loop(final List<Integer> data) {
    long output = 0;
    for (final Integer element: data) {
      if (element < 4) {
        continue;
      }
      output++;
    }
    System.out.println(output);
  }

  private static void pattern(final List<Integer> data) {
    long output = data.stream()
        .dropWhile(element -> element < 4)
        .count();
    System.out.println(output);
  }
}

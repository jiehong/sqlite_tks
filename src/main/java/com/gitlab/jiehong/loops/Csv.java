package com.gitlab.jiehong.loops;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Goal:
 * Read all orders
 * Calculate the average order price whose order single price is greater than or equal to 100.
 * Print the result.
 */
public class Csv {
  public static void main(String[] args) {
    var start = Instant.now();
    final List<List<String>> linesAndFields = readCsvFile("./orders.csv");
    var fileRead = Instant.now();
    System.out.println(computeLoop(linesAndFields));
    var loop = Instant.now();
    System.out.println(computeStream(linesAndFields));
    var stream = Instant.now();

    System.out.println(":: Reading file: " + Duration.between(start, fileRead).toMillis() + " ms");
    System.out.println(":: Using loops: " + Duration.between(fileRead, loop).toMillis() + " ms");
    System.out.println(":: Using stream: " + Duration.between(loop, stream).toMillis() + " ms");
  }

  private static double computeLoop(final List<List<String>> linesAndFields) {
    return 0.0;
  }

  private static double computeStream(final List<List<String>> linesAndFields) {
    return 0.0;
  }

  private static List<List<String>> readCsvFile(final String filename) {
    List<List<String>> records = new ArrayList<>();
    try (final CSVReader csvReader = new CSVReader(new FileReader(filename))) {
      String[] values;
      while ((values = csvReader.readNext()) != null) {
        records.add(Arrays.asList(values));
      }
    } catch (CsvValidationException | IOException e) {
      throw new RuntimeException(e);
    }
    return records;
  }
}

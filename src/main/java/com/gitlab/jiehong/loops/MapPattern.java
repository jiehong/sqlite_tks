package com.gitlab.jiehong.loops;

import java.util.List;

public class MapPattern {
  public static void main(String[] args) {
    var data = List.of("HELLO", "Is", "this", "Customer", "SUPPORT", "?");
    loop(data);
    pattern(data);
  }
  private static void loop(final List<String> data) {
    for (final String element: data) {
      final int lowerCase = element.length();
      System.out.println(lowerCase);
    }
  }

  private static void pattern(final List<String> data) {
    data.stream()
        .map(element -> element.length())
        .forEach(System.out::println);
  }
}

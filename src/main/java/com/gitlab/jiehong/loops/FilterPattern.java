package com.gitlab.jiehong.loops;

import java.util.List;

public class FilterPattern {
  public static void main(String[] args) {
    var data = List.of("HELLO", "", ",", "are", "you", "", "there", "??");
    loop(data);
    pattern(data);
  }

  private static void loop(final List<String> data) {
    for (final String element: data) {
      if (!element.isEmpty()) {
        System.out.println(element);
      }
    }
  }

  private static void pattern(final List<String> data) {
    data.stream()
        .filter(element -> !element.isEmpty())
        .forEach(System.out::println);
  }
}
